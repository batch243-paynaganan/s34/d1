// Use the 'require directive' to load the express module package

const express = require('express');

// Create an application using an express function
const app=express();
// For our application server to run, we need a port to listen to.
const port = 3000;
// Methods used from express.js are middlewares
// API management is one of the common applications of middlewares
// Allows your app to read json data
app.use(express.json())
// Allows your app to read data from forms
app.use(express.urlencoded({extended:true}))
// [SECTION] Routes
// Create a GET route
// Express has methods corresponding to each HTTP method
// Syntax: app.Method
// This route expects to receive a GET request at the base of the URI "/"
// The full base URI for our local application for this route will be at "http://localhost:3000"
app.get("/", (req, res)=>{
    res.send("Hello World")
})

// This route expects to receive a GET request at  URI "/hello"
/* app.get("/hello",(req,res)=>{
    res.send("Hello from the /hello endpoint")
}) */
// This route expects to receive a POST request at the URI "/hello"
// req.body contains the contents or data of the request body
app.use("/hello",(req,res)=>{
    res.send(`Hello there ${req.body.firstName} ${req.body.lastName}`);
})
// create and array to use as our mock database
// sign up route
// An array that will store our user objects when the "/signup" route is accessed
let users=[];
app.post("/signup",(req,res)=>{
    console.log(req.body)
    if(req.body.username!=="" && req.body.password!==""){
        users.push(req.body)
        console.log(users)
        res.send(`User ${req.body.username} is successfully registered!`)
    }else{
        res.send("Please input both username and password.")
    }
})
// The route expects to receive a put request at the URI "/change-password"
app.put("/change-password",(req,res)=>{
    let message;
    for(let i=0; i<users.length; i++){
        if (req.body.username==users[i].username){
            users[i].password = req.body.password;
            message=`User ${req.body.username}'s password has been updated.`
            break;
        // If no user was found
        }else{
            message = "User does not exist"
        }
    }
            res.send(message)
})
// if the username and password are not complete, an error message will be sent back to the client 

app.listen(port, ()=>console.log(`Server running at port ${port}`));